package com.interview.test.task.service;

import com.interview.test.task.model.BlogPost;

import java.util.List;

public interface BlogPostService {
    BlogPost create(BlogPost dto);

    BlogPost update(BlogPost dto);

    BlogPost delete(BlogPost dto);

    BlogPost getByTitle(String title);

    List<BlogPost> getAll();

}
