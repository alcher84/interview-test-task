package com.interview.test.task.web;

import com.interview.test.task.model.BlogPost;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/blogPosts")
public class BlogPostControllerImpl implements BlogPostController {
    
    @Override
    public BlogPost create(BlogPost request) {
        return null;
    }

    @Override
    public BlogPost update(String title, String content) {
        return null;
    }

    @Override
    public void delete(String title) {

    }

    @Override
    public BlogPost getByTitle(String title) {
        return null;
    }

    @Override
    public List<BlogPost> getAll() {
        return List.of();
    }
}
